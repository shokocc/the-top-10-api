import { Module } from '@nestjs/common';

import { ApiController } from './controllers/api.controller';

@Module({
  providers: [],
  controllers: [ApiController],
})
export class ApiModule {}
