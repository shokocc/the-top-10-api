import { Test, TestingModule } from '@nestjs/testing';
import { response } from 'express';
import { ApiController } from './api.controller';

describe('Api Controller', () => {
  let controller: ApiController;

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiController],
      providers: [],
    }).compile();

    controller = module.get<ApiController>(ApiController);
  });

  it('calling info method',async () => {
    let response = await controller.info();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });
});
