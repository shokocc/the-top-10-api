import { Controller, Get } from '@nestjs/common';
import { version, name } from '../../../../package.json';
import { CustomResponse } from '../../../util/custom-response';

@Controller(['/', 'api'])
export class ApiController {
  constructor() {}

  @Get()
  async info() {
    return new CustomResponse(200, 'Ok', { version, name });
  }
}
