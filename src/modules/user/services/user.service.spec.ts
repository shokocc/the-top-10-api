import { Test, TestingModule } from '@nestjs/testing';
import { PaginationQuery } from 'src/common/interfaces/pagination-query.interface';
import { User } from 'src/models/user';
import { UserService } from './user.service';

class UserServiceMock {
  findAll() {
    return [];
  }

  findUser(user: User, withPassword = false) {
    return user;
  }

  isEmailAvailable(email: string) {
    return true;
  }

  update(user: User, withPassword = false) {
    return user;
  }

  create(user: User, withPassword = false) {
    return user;
  }

  delete(id: number) {
    return id;
  }
}

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const UserServiceProvider = {
      provide: UserService,
      useClass: UserServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [UserServiceProvider],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  let withPassword = false;

  it('should call all users method', async () => {
    let params: PaginationQuery = {
      take: 10,
      skip: 0,
    };

    const findAll = jest.spyOn(service, 'findAll');
    service.findAll(params, withPassword);
    expect(findAll).toHaveBeenCalled();
  });

  it('should call find user by id method', async () => {
    let user: User = {
      id: 1,
    };

    const findUser = jest.spyOn(service, 'findUser');
    service.findUser(user, withPassword);
    expect(findUser).toHaveBeenCalled();
  });

  it('should call is mail available method', async () => {
    const isEmailAvailable = jest.spyOn(service, 'isEmailAvailable');
    service.isEmailAvailable('test@gmail.com');
    expect(isEmailAvailable).toHaveBeenCalled();
  });

  it('should call update user method', async () => {
    let user: User = {
      id: 1,
      email: 'test@gmail.com',
      password: '123456',
      role: 'USER',
    };

    const update = jest.spyOn(service, 'update');
    service.update(user, withPassword);
    expect(update).toHaveBeenCalled();
  });

  it('should call create user method', async () => {
    let user: User = {
      email: 'test@gmail.com',
      password: '123456',
      role: 'USER',
    };

    const create = jest.spyOn(service, 'create');
    service.create(user, withPassword);
    expect(create).toHaveBeenCalled();
  });

  it('should call delete user by id method', async () => {
    const deleteUser = jest.spyOn(service, 'delete');
    service.delete(1);
    expect(deleteUser).toHaveBeenCalled();
  });
});
