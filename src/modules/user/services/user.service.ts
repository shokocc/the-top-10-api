import { Injectable } from '@nestjs/common';

import { prisma } from '../../../prisma/client';

import { PaginationQuery } from 'src/common/interfaces/pagination-query.interface';
import { User } from 'src/models/user';
import { Role } from '@prisma/client';

@Injectable()
export class UserService {
  constructor() {}

  findAll(
    { take, skip, search, order }: PaginationQuery,
    withPassword = false,
  ) {
    const where = {
      player: {},
      role: {
        not: {
          equals: Role.ADMIN,
        },
      },
    };

    if (search && search['nickname'])
      where.player['nickname'] = { contains: search['nickname'] };
    if (search && search['status'])
      where.player['status'] = { equals: search['status'] };

    if (search && search['id'])
      where['id'] = { equals: parseInt(search['id']) };
    if (search && search['email'])
      where['email'] = { contains: search['email'] };

    const orderBy = {};

    if (order && order['dir'] && order['by'])
      orderBy[order['by']] = order['dir'] == 'desc' ? 'desc' : 'asc';

    return prisma.user.findMany({
      orderBy,
      select: {
        id: true,
        email: true,
        password: withPassword,
        role: true,
        player: true,
      },
      take,
      skip,
      where,
    });
  }

  findUser(user: User, withPassword = false) {
    return prisma.user.findFirst({
      select: {
        id: true,
        email: true,
        password: withPassword,
        role: true,
        player: true,
      },
      where: user,
    });
  }

  isEmailAvailable(email: string) {
    return prisma.user.findFirst({ where: { email } }).then((user) => !user);
  }

  create(user: User, withPassword = false) {
    return prisma.user.create({
      select: {
        id: true,
        email: true,
        password: withPassword,
        role: true,
        player: true,
      },
      data: {
        email: user.email,
        password: user.password,
        role: user.role,
      },
    });
  }

  update(user: User, withPassword = false) {
    return prisma.user.update({
      select: {
        id: true,
        email: true,
        password: withPassword,
        role: true,
        player: true,
      },
      where: {
        id: user.id,
      },
      data: {
        email: user.email,
        password: user.password,
        role: user.role,
      },
    });
  }

  delete(id: number) {
    return prisma.user.delete({
      where: {
        id,
      },
    });
  }
}
