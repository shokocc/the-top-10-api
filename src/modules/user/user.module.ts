import { Module } from '@nestjs/common';
//Services
import { UserService } from './services/user.service';
import { PlayerService } from '../../modules/player/services/player.service';
import { TokenService } from '../../modules/auth/services/token.service';
//Controllers
import { UserController } from './controllers/user.controller';

@Module({
  providers: [UserService, PlayerService, TokenService],
  controllers: [UserController],
})
export class UserModule {}
