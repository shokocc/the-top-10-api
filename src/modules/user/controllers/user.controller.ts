import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseInterceptors,
} from '@nestjs/common';

import { CustomResponse } from '../../../util/custom-response';
import { hash } from '../../../util/hash';

import { PaginationInterceptor } from '../../../common/interceptors/pagination.interceptor';
import { PaginationQuery } from '../../../common/interfaces/pagination-query.interface';
import { ParseIntPipe } from '../../../common/injectables/parse-int.pipe';
import { UserService } from '../../../modules/user/services/user.service';
import { PlayerService } from '../../../modules/player/services/player.service';

import { User } from '../../../models/user';
import { Roles } from '../../../common/decorators/roles.decorator';
import { Role } from '@prisma/client';

@Controller('api/users')
export class UserController {
  constructor(
    private _userService: UserService,
    private _playerService: PlayerService,
  ) {}

  @Get()
  @Roles(Role.ADMIN)
  @UseInterceptors(PaginationInterceptor)
  getUsers(@Query() params: PaginationQuery) {
    return this._userService
      .findAll(params)
      .then((users) => new CustomResponse(200, 'Ok', users, params));
  }

  @Get(':id')
  @Roles(Role.ADMIN)
  async getUserById(@Param('id', ParseIntPipe) id: number) {
    const user = await this._userService.findUser(User.parse({ id }));

    if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);

    return new CustomResponse(200, 'Ok', user);
  }

  @Get('/email/check')
  async checkEmailAvailability(@Query('email') email: string) {
    const isAvailable = await this._userService.isEmailAvailable(email);

    if (!isAvailable)
      throw new HttpException('Email is already taken', HttpStatus.BAD_REQUEST);

    return new CustomResponse(200, 'Ok');
  }

  @Post()
  @Roles(Role.ADMIN)
  async createUser(@Body() body: { user: User }) {
    const isEmailAvailable = await this._userService.isEmailAvailable(
      body.user.email,
    );

    if (!isEmailAvailable)
      throw new HttpException('Email is already taken', HttpStatus.BAD_REQUEST);

    body.user.password = hash(body.user.password);

    const user = await this._userService.create(body.user);

    return new CustomResponse(201, 'Ok', user);
  }

  @Put()
  @Roles(Role.ADMIN, Role.USER)
  async updateUser(@Req() req, @Body() body: { user: User }) {
    if (req.user) {
      if (req.user.role == Role.USER && body.user.id !== req.user.id)
        throw new HttpException('Not my user', HttpStatus.FORBIDDEN);
      delete body.user.role;
    }

    let user = await this._userService.findUser(
      User.parse({ id: body.user.id }),
    );
    if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);

    if (body.user.email && body.user.email !== user.email) {
      const isEmailAvailable = await this._userService.isEmailAvailable(
        body.user.email,
      );

      if (!isEmailAvailable)
        throw new HttpException(
          'Email is already taken',
          HttpStatus.BAD_REQUEST,
        );
    }

    body.user.password = hash(body.user.password);

    user = await this._userService.update(body.user);

    return new CustomResponse(200, 'Ok', user);
  }

  @Delete(':id')
  @Roles(Role.ADMIN)
  async deleteUser(@Param('id', ParseIntPipe) id: number) {
    const user = await this._userService.findUser(User.parse({ id }));
    if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);

    const player = await this._playerService.findPlayerByUserId(user.id);
    if (player) await this._playerService.delete(player.id);

    await this._userService.delete(user.id);

    return new CustomResponse(200, 'Ok');
  }
}
