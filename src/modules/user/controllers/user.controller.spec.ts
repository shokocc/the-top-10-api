import { Test, TestingModule } from '@nestjs/testing';
import { PaginationQuery } from 'src/common/interfaces/pagination-query.interface';
import { User } from 'src/models/user';
import { PlayerService } from 'src/modules/player/services/player.service';
import { UserService } from '../services/user.service';
import { UserController } from './user.controller';

describe('Users Controller', () => {
  let controller: UserController;
  let _userService: UserService;
  let _playerService: PlayerService;

  beforeEach(async () => {
    const userServiceProvider = {
      provide: UserService,
      useFactory: () => ({
        findAll: jest.fn(async (params) => []),
        findUser: jest.fn(async (id) => ({
          id,
          email: 'test@gmail.com',
          password: '123456',
        })),
        isEmailAvailable: jest.fn(async () => true),
        create: jest.fn(async (user) => ({ id: 1, ...user })),
        update: jest.fn(async (user) => ({ id: 1, ...user })),
        delete: jest.fn(async () => {}),
      }),
    };

    const playerServiceProvider = {
      provide: PlayerService,
      useFactory: () => ({
        delete: jest.fn(async (id) => id),
        findPlayerByUserId: jest.fn(async (user_id) => user_id),
      }),
    };


    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [userServiceProvider, playerServiceProvider],
    }).compile();

    controller = module.get<UserController>(UserController);
    _userService = module.get<UserService>(UserService);
    _playerService = module.get<PlayerService>(PlayerService);
  });

  it('calling get all users method', async() => {
    let params: PaginationQuery = {
      take: 10,
      skip: 0,
    };
    let response = await controller.getUsers(params);
    expect(_userService.findAll).toHaveBeenCalledWith(params);
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling get user by id method', async() => {
    let userId = 1;
    let response = await controller.getUserById(userId);
    expect(_userService.findUser).toHaveBeenCalledWith(User.parse({
      id: userId
    }));
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling check email availability method', async () => {
    let response = await controller.checkEmailAvailability('test_user@gmail.com');
    expect(_userService.isEmailAvailable).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling create user method', async () => {

    const body ={
      user: User.parse({
        email: "test@gmail.com",
        password: "123456",
        role: "USER"
      }),
    };
  
    let response = await controller.createUser(body);
    expect(_userService.create).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:201, message:'Ok'});
  });

  it('calling update user method', async () => {

    const req = {
      user: {
        id:2,
        email: 'test@gmail.com',
        password: '123456',
        player: {
          id: 3,
          nickname: 'Test',
          ranking: 100,
          avatar_url: 'google.com',
        },
        role: 'TEST',
      },
    };

    const body ={
      user: User.parse({
        id: 1,
        email: "test@gmail.com",
        password: "123456",
        role: "USER"
      }),
    };
  
    let response = await controller.updateUser(req,body);
    expect(_userService.update).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling delete user by id method', async () => {
    let response = await controller.deleteUser(1);
    expect(_userService.delete).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

});
