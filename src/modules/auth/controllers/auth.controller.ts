import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  Req,
} from '@nestjs/common';
import { Role, Status } from '@prisma/client';

import { CustomResponse } from '../../../util/custom-response';
import { hash } from '../../../util/hash';
import { encrypt } from '../../../util/jwt';

import { PlayerService } from '../../../modules/player/services/player.service';
import { UserService } from '../../../modules/user/services/user.service';
import { TokenService } from '../../../modules/auth/services/token.service';

import { User } from '../../../models/user';
import { Player } from '../../../models/player';
import { Token } from '../../../models/token';

@Controller('api/auth')
export class AuthController {
  constructor(
    private _tokenService: TokenService,
    private _userService: UserService,
    private _playerService: PlayerService,
  ) {}

  @Post('register')
  async register(@Body() body: { player: Player; user: User }) {
    const isEmailAvailable = await this._userService.isEmailAvailable(
      body.user.email,
    );

    if (!isEmailAvailable)
      throw new HttpException(
        'Email is already taken',
        HttpStatus.BAD_REQUEST,
      );

    const isNicknameAvailable = await this._playerService.isNicknameAvailable(
      body.player.nickname,
    );

    if (!isNicknameAvailable)
      throw new HttpException(
        'Nickname is already taken',
        HttpStatus.BAD_REQUEST,
      );

    body.user.role = Role.USER;
    body.user.password = hash(body.user.password);

    const user = await this._userService.create(body.user);

    body.player.user_id = user.id;
    body.player.status = Status.BRONZE;

    const player = await this._playerService.create(body.player);

    return new CustomResponse(201, 'Ok', { user, player });
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() body: { email: string; password: string }) {

    const isEmailAvailable = await this._userService.isEmailAvailable(
      body.email,
    );

    if (isEmailAvailable)
      throw new HttpException('Email not registered', HttpStatus.BAD_REQUEST);

    body.password = hash(body.password);

    const user = await this._userService.findUser(
      User.parse({ email: body.email, password: body.password }),
    );

    if (!user)
      throw new HttpException('Wrong password', HttpStatus.BAD_REQUEST);

    const access_token = encrypt({ user_id: user.id, password: body.password });

    await this._tokenService.deleteTokensFromUserId(user.id);

    await this._tokenService.create(
      Token.parse({ access_token, user_id: user.id }),
    );

    return new CustomResponse(200, 'Ok', { access_token, user });
  }

  @Get('token/check')
  async verifyToken(@Req() request) {
    const { access_token, user } = request;

    return new CustomResponse(200, 'Ok', { access_token, user });
  }
}
