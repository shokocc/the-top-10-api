import { Test, TestingModule } from '@nestjs/testing';
import { Player } from 'src/models/player';
import { User } from 'src/models/user';
import { PlayerService } from 'src/modules/player/services/player.service';
import { UserService } from 'src/modules/user/services/user.service';
import { TokenService } from '../services/token.service';
import { AuthController } from './auth.controller';


describe('Auth Controller', () => {
  let controller: AuthController;
  let _tokenService: TokenService;
  let _userService: UserService;
  let _playerService: PlayerService;

  beforeEach(async () => {

    const tokenServiceProvider = {
      provide: TokenService,
      useFactory: () => ({
        create: jest.fn(async (token) => token),
        deleteTokensFromUserId: jest.fn(async (user_id) => user_id)
      }),
    };

    const userServiceProvider = {
      provide: UserService,
      useFactory: () => ({
        isEmailAvailable: jest.fn(async (email) => { 
          if(email == 'test_login@gmail.com') return false;
          return true;
        }),
        create: jest.fn(async (user) => ({ id: 1, ...user })),
        findUser: jest.fn(async (id) => ({
          id,
          email: 'test@gmail.com',
          password: '123456',
        })),
      }),
    };
    
    const playerServiceProvider = {
      provide: PlayerService,
      useFactory: () => ({
        isNicknameAvailable: jest.fn(async () => true),
        create: jest.fn(async (player) => ({ id: 1, ...player })),
      }),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [tokenServiceProvider, userServiceProvider, playerServiceProvider],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    _tokenService= module.get<TokenService>(TokenService);
    _userService= module.get<UserService>(UserService);
    _playerService= module.get<PlayerService>(PlayerService);
  });

  it('calling register auth method', async () => {
    const body = {
      player: Player.parse({
        id: 1,
        nickname: 'Test',
        ranking: 100,
      }),
      user: User.parse({
        email: "test@gmail.com",
        password: "123456"
      }),
    };
    const response = await controller.register(body);
    expect(response).toMatchObject({statusCode:201, message:'Ok'});
  });

  it('calling login auth method', async () => {
    const body = {
        email: "test_login@gmail.com",
        password: "123456"
    };
    const response = await controller.login(body);
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling verify token auth method', async () => {
    
    const req = {
      access_token:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxODMsInBhc3N3b3JkIjoiOGQ5NjllZWY2ZWNhZDNjMjlhM2E2MjkyODBlNjg2Y2YwYzNmNWQ1YTg2YWZmM2NhMTIwMjBjOTIzYWRjNmM5MiIsImlhdCI6MTYzNzc5ODgyMiwiZXhwIjoxNjM3ODQyMDIyfQ.xTm2knYJcEXMmgWfkfyynVxII0c62xjdumhpH9sYTTE',
      user: {
        email: "test@gmail.com",
        password: "123456"
      }
    }
    const response = await controller.verifyToken(req);
    expect(response).toMatchObject({statusCode:200, message:'Ok',data: req});
  });


});
