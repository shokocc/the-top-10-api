import { Module } from '@nestjs/common';

import { UserService } from '../../modules/user/services/user.service';
import { PlayerService } from '../../modules/player/services/player.service';
import { TokenService } from './services/token.service';

import { AuthController } from './controllers/auth.controller';
import { AuthGuard } from '../../common/guards/auth.guard';

@Module({
  providers: [UserService, PlayerService, TokenService, AuthGuard],
  controllers: [AuthController],
})
export class AuthModule {}
