import { Injectable } from '@nestjs/common';
import { Token } from 'src/models/token';

import { prisma } from '../../../prisma/client';

@Injectable()
export class TokenService {
  constructor() {}

  //Método consultar player
  findAll() {
    return prisma.token.findMany();
  }

  findToken(access_token: string) {
    return prisma.token.findUnique({
      where: {
        access_token,
      },
    });
  }

  create(token: Token) {
    return prisma.token.create({
      data: {
        access_token: token.access_token,
        user_id: token.user_id,
      },
    });
  }

  delete(access_token: string) {
    return prisma.token.delete({
      where: {
        access_token,
      },
    });
  }

  deleteTokensFromUserId(user_id: number) {
    return prisma.token.deleteMany({
      where: {
        user_id,
      },
    });
  }
}
