import { Token } from '.prisma/client';
import { Test, TestingModule } from '@nestjs/testing';
import { TokenService } from './token.service';

class TokenServiceMock {
  findAll() {
    return [];
  }

  findToken(access_token: string) {
    return access_token;
  }

  create(token: Token) {
    return token;
  }

  delete(access_token: string) {
    return access_token;
  }

  deleteTokensFromUserId(user_id: number) {
    return user_id;
  }

}

describe('TokenService', () => {
  let service: TokenService;

  beforeEach(async () => {

    const TokenServiceProvider = {
      provide: TokenService,
      useClass: TokenServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [TokenServiceProvider],
    }).compile();

    service = module.get<TokenService>(TokenService);
    
  });
  let access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxODMsInBhc3N3b3JkIjoiOGQ5NjllZWY2ZWNhZDNjMjlhM2E2MjkyODBlNjg2Y2YwYzNmNWQ1YTg2YWZmM2NhMTIwMjBjOTIzYWRjNmM5MiIsImlhdCI6MTYzNzc5ODgyMiwiZXhwIjoxNjM3ODQyMDIyfQ.xTm2knYJcEXMmgWfkfyynVxII0c62xjdumhpH9sYTTE';

  it('should call all tokens method', async () => {
    const findAll = jest.spyOn(service, 'findAll');
    service.findAll();
    expect(findAll).toHaveBeenCalled();
  });

  it('should call find token by access token method', async () => {
    const findToken = jest.spyOn(service, 'findToken');
    
    service.findToken(access_token);
    expect(findToken).toHaveBeenCalled();
    expect(findToken).toHaveBeenCalledWith(access_token);
  });

  it('should call create token method', async () => {
    const create = jest.spyOn(service, 'create');
    let token : Token={
      access_token :access_token,
      user_id:1,
      created_at:new Date(),
      updated_at: new Date()
    }
    service.create(token);
    expect(create).toHaveBeenCalled();
    expect(create).toHaveBeenCalledWith(token);
  });

  it('should call delete token method', async () => {
    const deleteToken = jest.spyOn(service, 'delete');
    service.delete(access_token);
    expect(deleteToken).toHaveBeenCalled();
    expect(deleteToken).toHaveBeenCalledWith(access_token);
  });

  it('should call deletes token from user id method', async () => {
    const deleteTokensFromUserId = jest.spyOn(service, 'deleteTokensFromUserId');
    let userId = 1;
    service.deleteTokensFromUserId(userId);
    expect(deleteTokensFromUserId).toHaveBeenCalled();
    expect(deleteTokensFromUserId).toHaveBeenCalledWith(userId);
  });


});
