import { Test, TestingModule } from '@nestjs/testing';
import { PaginationQuery } from 'src/common/interfaces/pagination-query.interface';
import { Player } from 'src/models/player';
import { PlayerService } from './player.service';

class PlayerServiceMock {
  create(player: Player) {
    return player;
  }

  update(player: Player) {
    return player;
  }

  findAll({ take, skip }: PaginationQuery) {
    return [];
  }

  findPlayer(player: Player) {
    return player;
  }

  findPlayerByUserId(user_id: number) {
    return {};
  }

  isNicknameAvailable(nickname: string) {
    return true;
  }

  delete(id: number) {
    return id;
  }
}

describe('PlayerService', () => {
  let service: PlayerService;

  beforeEach(async () => {
    const PlayerServiceProvider = {
      provide: PlayerService,
      useClass: PlayerServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [PlayerService, PlayerServiceProvider],
    }).compile();

    service = module.get<PlayerService>(PlayerService);
  });

  it('should call create player method', async () => {
    let player: Player = {
      nickname: 'Juan',
      status: 'BRONZE',
      ranking: 100,
      avatar_url: 'google.com',
      user_id: 8,
    };

    const createPlayer = jest.spyOn(service, 'create');
    service.create(player);
    expect(createPlayer).toHaveBeenCalledWith(player);
  });

  it('should call update player method', async () => {
    let player: Player = {
      id: 1,
      nickname: 'Juan',
      status: 'BRONZE',
      ranking: 100,
      avatar_url: 'google.com',
      user_id: 8,
    };

    const updatePlayer = jest.spyOn(service, 'update');
    service.update(player);
    expect(updatePlayer).toHaveBeenCalledWith(player);
  });

  it('should call all players method', async () => {
    let params: PaginationQuery = {
      take: 10,
      skip: 0,
    };

    const getAllPlayers = jest.spyOn(service, 'findAll');
    service.findAll(params);
    expect(getAllPlayers).toHaveBeenCalledWith(params);
  });

  it('should call find player with id method ', async () => {
    const findPlayer = jest.spyOn(service, 'findPlayer');

    let player: Player = {
      id: 1,
    };
    service.findPlayer(player);
    expect(findPlayer).toHaveBeenCalledWith(player);
  });

  it('should call find player by user id method ', async () => {
    const findPlayerByUserId = jest.spyOn(service, 'findPlayerByUserId');

    service.findPlayerByUserId(1);
    expect(findPlayerByUserId).toHaveBeenCalledWith(1);
  });

  it('should call nickname available method ', async () => {
    const isNicknameAvailable = jest.spyOn(service, 'isNicknameAvailable');

    service.isNicknameAvailable("test");
    expect(isNicknameAvailable).toHaveBeenCalledWith("test");
  });

  it('should call delete player by id method', async () => {
    const deletePlayer = jest.spyOn(service, 'delete');
    service.delete(1);
    expect(deletePlayer).toHaveBeenCalledWith(1);
  });
});
