import { Injectable } from '@nestjs/common';

import { prisma } from '../../../prisma/client';

import { PaginationQuery } from 'src/common/interfaces/pagination-query.interface';
import { Player } from 'src/models/player';

@Injectable()
export class PlayerService {
  constructor() {}

  findAll({ take, skip, search, order }: PaginationQuery) {
    const where = {};

    if (search && search['id'])
      where['id'] = { equals: parseInt(search['id']) };
    if (search && search['nickname'])
      where['nickname'] = { contains: search['nickname'] };
    if (search && search['status'])
      where['status'] = { equals: search['status'] };

    const orderBy = {};

    if (order && order['dir'] && order['by'])
      orderBy[order['by']] = order['dir'] == 'desc' ? 'desc' : 'asc';
    else orderBy['ranking'] = 'desc';

    return prisma.player.findMany({
      orderBy,
      take,
      skip,
      where,
    });
  }

  findPlayer(player: Player) {
    return prisma.player.findFirst({
      where: player,
    });
  }

  findPlayerByUserId(user_id: number) {
    return prisma.player.findUnique({
      where: {
        user_id,
      },
    });
  }

  isNicknameAvailable(nickname: string) {
    return prisma.player
      .findFirst({ where: { nickname } })
      .then((player) => !player);
  }

  create(player: Player) {
    return prisma.player.create({
      data: {
        nickname: player.nickname,
        status: player.status,
        ranking: player.ranking,
        avatar_url: player.avatar_url,
        user_id: player.user_id,
      },
    });
  }

  update(player: Player) {
    return prisma.player.update({
      where: {
        id: player.id,
      },
      data: {
        nickname: player.nickname,
        status: player.status,
        ranking: player.ranking,
        avatar_url: player.avatar_url,
        user_id: player.user_id,
      },
    });
  }

  delete(id: number) {
    return prisma.player.delete({
      where: {
        id,
      },
    });
  }
}
