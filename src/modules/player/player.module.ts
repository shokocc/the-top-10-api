import { Module } from '@nestjs/common';
//Services
import { PlayerService } from './services/player.service';
import { UserService } from '../../modules/user/services/user.service';
import { TokenService } from '../../modules/auth/services/token.service';
//Controllers
import { PlayerController } from './controllers/player.controller';

@Module({
  providers: [PlayerService, UserService, TokenService],
  controllers: [PlayerController],
})
export class PlayerModule {}
