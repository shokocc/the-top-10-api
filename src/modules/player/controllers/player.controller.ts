import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseInterceptors,
} from '@nestjs/common';

import { CustomResponse } from '../../../util/custom-response';
import { PaginationInterceptor } from '../../../common/interceptors/pagination.interceptor';
import { PaginationQuery } from '../../../common/interfaces/pagination-query.interface';
import { ParseIntPipe } from '../../../common/injectables/parse-int.pipe';
import { PlayerService } from '../../../modules/player/services/player.service';
import { UserService } from '../../../modules/user/services/user.service';

import { Player } from '../../../models/player';
import { User } from '../../../models/user';
import { Roles } from '../../../common/decorators/roles.decorator';
import { Role } from '@prisma/client';

@Controller('api/players')
export class PlayerController {
  constructor(
    private _playerService: PlayerService,
    private _userService: UserService,
  ) {}

  @Get()
  @UseInterceptors(PaginationInterceptor)
  getPlayer(@Query() params: PaginationQuery) {
    return this._playerService
      .findAll(params)
      .then((players) => new CustomResponse(200, 'Ok', players, params));
  }

  @Get(':id')
  async getPlayerById(@Param('id', ParseIntPipe) id: number) {
    const player = await this._playerService.findPlayer(Player.parse({ id }));
    if (!player)
      throw new HttpException('Player not found', HttpStatus.NOT_FOUND);

    return new CustomResponse(200, 'Ok', player);
  }

  @Get('/nickname/check')
  async checkNicknameAvailability(@Query('nickname') nickname: string) {
    const isAvailable = await this._playerService.isNicknameAvailable(nickname);

    if (!isAvailable)
      throw new HttpException(
        'Nickname is already taken',
        HttpStatus.BAD_REQUEST,
      );

    return new CustomResponse(200, 'Ok');
  }

  @Post()
  @Roles(Role.ADMIN)
  async createPlayer(@Body() body: { player: Player }) {
    const isNicknameAvailable = await this._playerService.isNicknameAvailable(
      body.player.nickname,
    );

    if (!isNicknameAvailable)
      throw new HttpException(
        'Nickname is already taken',
        HttpStatus.BAD_REQUEST,
      );

    const user = await this._userService.findUser(
      User.parse({ id: body.player.user_id }),
    );
    if (!user) throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    if (user.player)
      throw new HttpException(
        'User already has a player',
        HttpStatus.BAD_REQUEST,
      );

    const player = await this._playerService.create(body.player);

    return new CustomResponse(201, 'Ok', player);
  }

  @Put()
  @Roles(Role.ADMIN, Role.USER)
  async updatePlayer(@Req() req, @Body() body: { player: Player }) {
    let player = await this._playerService.findPlayer(
      Player.parse({ id: body.player.id }),
    );
    if (!player)
      throw new HttpException('Player not found', HttpStatus.NOT_FOUND);
    if (req.user && req.user.role === Role.USER) {
      if (player.user_id !== req.user.id)
        throw new HttpException('Not my user', HttpStatus.FORBIDDEN);
      delete body.player.status;
      delete body.player.user_id;
    }
    if (body.player.nickname && body.player.nickname !== player.nickname) {
      const isNicknameAvailable = await this._playerService.isNicknameAvailable(
        body.player.nickname,
      );

      if (!isNicknameAvailable)
        throw new HttpException(
          'Nickname is already taken',
          HttpStatus.BAD_REQUEST,
        );
    }

    if (body.player.user_id) {
      const user = await this._userService.findUser(
        User.parse({ id: body.player.user_id }),
      );
      if (!user)
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      if (user.player && user.player.id !== body.player.id)
        throw new HttpException(
          'User already has a player',
          HttpStatus.BAD_REQUEST,
        );
    }

    player = await this._playerService.update(body.player);

    return new CustomResponse(200, 'Ok', player);
  }

  @Delete(':id')
  @Roles(Role.ADMIN)
  async deletePlayer(@Param('id', ParseIntPipe) id: number) {
    const player = await this._playerService.findPlayer(Player.parse({ id }));

    if (!player)
      throw new HttpException('Player not found', HttpStatus.NOT_FOUND);

    await this._playerService.delete(player.id);
    await this._userService.delete(player.user_id);

    return new CustomResponse(200, 'Ok');
  }
}
