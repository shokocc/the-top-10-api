import { Test, TestingModule } from '@nestjs/testing';
import { PlayerService } from '../services/player.service';
import { UserService } from '../../../modules/user/services/user.service';
import { PlayerController } from './player.controller';
import { Player } from 'src/models/player';
import { PaginationQuery } from 'src/common/interfaces/pagination-query.interface';

describe('Players Controller', () => {
  let controller: PlayerController;
  let _playerService: PlayerService;
  
  beforeAll(async () => {
    const playerServiceProvider = {
      provide: PlayerService,
      useFactory: () => ({
        findAll: jest.fn(async (params) => []),
        findPlayer: jest.fn(async (player) => player),
        create: jest.fn(async (player) => ({ id: 1, ...player })),
        update: jest.fn(async (player) => player),
        isNicknameAvailable: jest.fn(async () => true),
        delete: jest.fn(async (id) => id),
      }),
    };

    const userServiceProvider = {
      provide: UserService,
      useFactory: () => ({
        isEmailAvailable: jest.fn(async () => true),
        create: jest.fn(async (user) => ({ id: 1, ...user })),
        findUser: jest.fn(async (id) => ({
          id,
          email: 'test@gmail.com',
          password: '123456',
        })),
        delete: jest.fn(async () => {}),
        update: jest.fn(async (user) => ({ id: 1, ...user })),
      }),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlayerController],
      providers: [playerServiceProvider, userServiceProvider],
    }).compile();

    controller = module.get<PlayerController>(PlayerController);
    _playerService = module.get<PlayerService>(PlayerService);
  });

  it('calling save Player method', async () => {
    const body = {
      player: {
        user_id: 1,
        nickname: 'Test',
        ranking: 100,
        avatar_url: 'google.com',
      },
    };
    const response = await controller.createPlayer(body);
    expect(_playerService.create).toHaveBeenCalled();
    expect(response.data).toMatchObject(Player.parse(body.player));
  });

  it('calling getAllPlayers method', () => {
    let params: PaginationQuery = {
      take: 10,
      skip: 0,
    };

    controller.getPlayer(params);
    expect(_playerService.findAll).toHaveBeenCalledWith(params);
  });

  it('calling get player by id method', () => {
    controller.getPlayerById(1);
    expect(_playerService.findPlayer).toHaveBeenCalled();
  });

  it('calling update player method', async () => {
    const req = {
      user: {
        email: 'test@gmail.com',
        password: '123456',
        player: {
          id: 3,
          nickname: 'Test',
          ranking: 100,
          avatar_url: 'google.com',
        },
        role: 'USER',
      },
    };

    const body = {
      player: Player.parse({
        id: 1,
        nickname: 'Juan',
        status: 'BRONZE',
        ranking: 100,
        avatar_url: 'google.com',
        user_id: 1,
      }),
    };

    let response = await controller.updatePlayer(req, body);
    expect(_playerService.update).toHaveBeenCalled();
    expect(_playerService.update).toHaveBeenCalledWith(body.player);
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling delete player by id method', async () => {
    let response = await controller.deletePlayer(1);
    expect(_playerService.delete).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling check nickname availability method', async () => {
    let response = await controller.checkNicknameAvailability('tests');
    expect(_playerService.isNicknameAvailable).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });

  it('calling delete player by id method', async () => {
    let response = await controller.deletePlayer(1);
    expect(_playerService.delete).toHaveBeenCalled();
    expect(response).toMatchObject({statusCode:200, message:'Ok'});
  });
});
