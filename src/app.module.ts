import { Module } from '@nestjs/common';
import { LoggerModule } from 'nestjs-pino';

import { PlayerModule } from './modules/player/player.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { ApiModule } from './modules/api/api.module';

@Module({
  imports: [
    // LoggerModule.forRoot(),
    ApiModule,
    PlayerModule,
    UserModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
