import * as jwt from 'jsonwebtoken';

export function encrypt(value: any) {
  return jwt.sign(value, process.env.JWT_SECRET, { expiresIn: '12h' });
}

export function decrypt(token: string): any {
  return jwt.verify(token, process.env.JWT_SECRET);
}
