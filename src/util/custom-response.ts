import { ResponseHttp } from 'src/common/interfaces/http-response.interface';

export class CustomResponse implements ResponseHttp {
  statusCode: number;
  message?: string;
  meta?: any;
  data?: any;

  constructor(statusCode: number, message?: string, data?: any, meta?: any) {
    this.statusCode = statusCode;
    if (message) this.message = message;
    if (meta) this.meta = meta;
    if (data) this.data = data;
  }
}
