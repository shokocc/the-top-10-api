import { PrismaClient } from '@prisma/client';

const options = {};

if (process.env.NODE_ENV === 'production') {
  options['log'] = ['query', 'info'];
}

export const prisma = new PrismaClient({ ...options });
