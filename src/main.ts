import { NestFactory } from '@nestjs/core';
import { Logger } from 'nestjs-pino';
import * as cookieParser from 'cookie-parser';

import { AppModule } from './app.module';
import { AuthModule } from './modules/auth/auth.module';
import { AuthGuard } from './common/guards/auth.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });

  app.use(cookieParser());
  if (process.env.NODE_ENV === 'production') app.useLogger(app.get(Logger));

  const whitelist = (process.env.CORS_WHITELIST + '')
    .split(',')
    .map((v) => v.trim())
    .filter((v) => !!v);

  if (whitelist.length > 0) {
    app.enableCors({
      origin: function (origin, callback) {
        if (origin === undefined || whitelist.indexOf(origin) !== -1) {
          console.info('Allowed cors for:', origin);
          callback(null, true);
        } else {
          console.info('Blocked cors for:', origin);
          callback(new Error('Not allowed by CORS'));
        }
      },
      allowedHeaders:
        'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
      methods: 'GET,PUT,POST,DELETE,UPDATE,OPTIONS',
      credentials: true,
    });
  } else {
    app.enableCors();
  }

  const authGuard = app.select(AuthModule).get(AuthGuard);
  app.useGlobalGuards(authGuard);

  await app.listen(3000);
}

bootstrap();
