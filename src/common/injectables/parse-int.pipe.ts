import { BadRequestException, PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class ParseIntPipe implements PipeTransform<string> {
  transform(value: string) {
    const val = parseInt(value);

    if (isNaN(val))
      throw new BadRequestException('Validation failed, must be Integer');

    return val;
  }
}
