import { ParseIntPipe } from './parse-int.pipe';

describe('ParseIntPipe', () => {
  const pipe = new ParseIntPipe();

  it('pipe to be defined', () => {
    expect(pipe).toBeDefined();
  });

  it('transforms "0" to 0', () => {
    expect(pipe.transform('0')).toBe(0);
  });

  it('transforms "1.5" to 1', () => {
    expect(pipe.transform('1.5')).toBe(1);
  });

  it('transforms str untrimmed "  30  " to 30', () => {
    expect(pipe.transform('  30  ')).toBe(30);
  });

  it('transforms "-50" to -50', () => {
    expect(pipe.transform('-50')).toBe(-50);
  });

  it('throws if input is "a"', () => {
    expect(() => {
      pipe.transform('a');
    }).toThrowError('Validation failed, must be Integer');
  });

  it('throws if input is ""', () => {
    expect(() => {
      pipe.transform('');
    }).toThrowError('Validation failed, must be Integer');
  });

  it('throws if input is " "', () => {
    expect(() => {
      pipe.transform(' ');
    }).toThrowError('Validation failed, must be Integer');
  });

  it('throws if input is null', () => {
    expect(() => {
      pipe.transform(null);
    }).toThrowError('Validation failed, must be Integer');
  });
});
