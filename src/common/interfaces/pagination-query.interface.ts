export interface PaginationQuery {
  take: number;
  skip: number;
  search: {
    [key: string]: string;
  };
  order: {
    by: string;
    dir: string;
  };
}
