export interface ResponseHttp {
  statusCode: number;
  message?: string;
  meta?: any;
  data?: any;
}
