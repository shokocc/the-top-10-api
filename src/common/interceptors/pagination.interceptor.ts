import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class PaginationInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();

    request.query.take = parseInt(request.query?.take || 10);
    request.query.skip = parseInt(request.query?.skip || 0);

    if (isNaN(request.query.take)) request.query.take = 10;
    if (isNaN(request.query.skip)) request.query.skip = 0;

    if (request.query.skip < 0) request.query.skip = 0;
    if (request.query.take < 1) request.query.take = 1;
    if (request.query.take > 50) request.query.take = 50;

    return next.handle();
  }
}
