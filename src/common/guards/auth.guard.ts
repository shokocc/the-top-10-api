import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

import { decrypt } from '../../util/jwt';
import { TokenService } from '../../modules/auth/services/token.service';
import { UserService } from '../../modules/user/services/user.service';

import { User } from '../../models/user';
import { Reflector } from '@nestjs/core';
import { Role } from '@prisma/client';

@Injectable()
export class AuthGuard implements CanActivate {
  tokenService: TokenService;
  userService: UserService;

  constructor(private reflector: Reflector) {
    this.tokenService = new TokenService();
    this.userService = new UserService();
  }

  async canActivate(context: ExecutionContext) {
    const request: Request = context.switchToHttp().getRequest();
    const roles = this.reflector.get<Role[]>('roles', context.getHandler());

    const useToken =
      request.headers['authorization'] &&
      request.headers['authorization'].startsWith('Bearer ');

    if (useToken) {
      const access_token = (
        request.headers['authorization'].split('Bearer ')[1] + ''
      ).trim();

      const token = await this.tokenService.findToken(access_token);
      if (!token) return false;

      const tokenData = decrypt(access_token);
      if (!tokenData) return false;

      const user = await this.userService.findUser(
        User.parse({ id: token.user_id, password: tokenData.password }),
      );
      if (!user) return false;

      request['user'] = user;
      request['access_token'] = access_token;
    }

    if (roles) {
      const hasRole = roles.includes(request['user'] && request['user'].role);
      if (!hasRole) return false;
    }

    return true;
  }
}
