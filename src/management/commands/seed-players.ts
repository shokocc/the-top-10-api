import mocker from 'mocker-data-generator';

import { Status, Role } from '@prisma/client';

import { prisma } from '../../prisma/client';
import { hash } from '../../util/hash';

let playerAmount = 3000;

if (process.argv[2]) {
  try {
    playerAmount = parseInt(process.argv[2]);
  } catch (error) {
    // Default value will be used
  }
}

const defaultPassword = hash('123456');

const playerMockBase = {
  firstName: {
    faker: 'name.firstName',
  },
  lastName: {
    faker: 'name.lastName',
  },
  status: {
    static: Status.BRONZE,
  },
  email: {
    casual: 'email',
  },
  password: {
    static: defaultPassword,
  },
  role: {
    static: Role.USER,
  },
  avatar_id: {
    faker: 'datatype.number({"min": 1, "max": 1000})',
  },
  ranking: {
    faker: 'datatype.number({"min": 1, "max": 1000})',
  },
  nickname: {
    function: function () {
      return (
        this.object.lastName.substring(0, 5) +
        this.object.firstName.substring(0, 3) +
        Math.floor(Math.random() * 10)
      );
    },
  },
  user: {
    function: function () {
      return {
        email: this.object.email,
        password: this.object.password,
        role: this.object.role,
      };
    },
  },
  player: {
    function: function () {
      return {
        nickname: this.object.nickname,
        status: this.object.status,
        ranking: this.object.ranking,
        avatar_url: `https://picsum.photos/id/${this.object.avatar_id}/200/300`,
      };
    },
  },
};

console.log(`Generating ${playerAmount} Mock Players...`);

async function uploadPlayers(players) {
  try {
    for (let i = 0; i < players.length; i++) {
      console.log(`Uploading ${i + 1} of ${players.length}`);
      const { player, user } = players[i];
      try {
        await prisma.player.create({
          data: {
            ...player,
            user: {
              create: user,
            },
          },
        });
      } catch (error) {
        console.error(error);
        console.log('Failed');
      }
    }
    console.log('Done');
  } catch (error) {
    console.log(error);
    console.log('Errored');
  }
}

mocker()
  .schema('players', playerMockBase, playerAmount)
  .build((error, { players }) => {
    if (error) throw error;
    console.log(`Generated ${players.length} Mock Players, Uploading...`);
    uploadPlayers(players);
  });
