import * as path from 'path';
import * as fs from 'fs';
import * as dotenv from 'dotenv';

const envPath = path.join(process.cwd(), '.env.test');

const envConfig = dotenv.parse(fs.readFileSync(envPath));

for (const k in envConfig) {
  process.env[k] = envConfig[k];
}

import { spawn } from 'child_process';
import { createAdmin } from './create-admin';

export function setupTestDb() {
  return new Promise<void>((resolve, reject) => {
    const args = process.argv.slice(2);

    args.shift();

    process.env['NODE_ENV'] = 'test';

    function spawnPrisma(args) {
      if (process.platform === 'win32') {
        return spawn('cmd', ['/c', 'node_modules\\.bin\\prisma.cmd', ...args], {
          stdio: 'inherit',
        });
      } else {
        return spawn('node_modules/.bin/prisma', args, { stdio: 'inherit' });
      }
    }

    const migrate = spawnPrisma(['migrate', 'reset', '--force']);

    migrate.on('close', () => {
      createAdmin('admin@gmail.com', '123456', true)
        .catch((err) => {
          reject(err);
        })
        .finally(() => {
          resolve();
        });
    });

    migrate.on('error', (err) => {
      reject(err);
    });
  });
}

if (require.main === module) {
  setupTestDb()
    .then(() => {
      console.log('Done');
    })
    .catch((err) => {
      console.error(err);
    });
}
