import { Role } from '@prisma/client';

import { prisma } from '../../prisma/client';
import { hash } from '../../util/hash';

export async function createAdmin(email, raw_password, silent = true) {
  try {
    if (!email.trim() || !raw_password.trim()) {
      console.log('Please provide email and password');
      return;
    }

    const password = hash(raw_password);

    let user = await prisma.user.findFirst({ where: { email } });
    if (user) {
      throw 'User EMAIL is already in use.';
    }
    if (!silent) console.log(`Generating user...`);

    user = await prisma.user.create({
      data: {
        email,
        password,
        role: Role.ADMIN,
      },
    });

    if (!silent) {
      console.log(`User Created...`);
      console.log(`\nID: ${user.id}`);
      console.log(`EMAIL: ${user.email}`);
      console.log(`ROLE: ${user.role}\n`);

      console.log('Done');
    }
  } catch (error) {
    throw error;
  }
}

if (require.main === module) {
  const email = process.argv[2] || '';
  const raw_password = process.argv[3] || '';
  createAdmin(email, raw_password, false)
    .then(() => {
      console.log('Done');
    })
    .catch((err) => {
      console.error(err);
    });
}
