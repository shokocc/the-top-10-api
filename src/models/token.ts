export class Token {
  access_token?: string;
  user_id?: number;

  static parse(obj: any) {
    const token = new Token();

    if (obj.access_token) token.access_token = obj.access_token;
    if (obj.user_id) token.user_id = obj.user_id;

    return token;
  }
}
