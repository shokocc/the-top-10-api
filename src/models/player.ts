import { Status } from '@prisma/client';

export class Player {
  id?: number;

  nickname?: string;
  status?: Status;
  ranking?: number;
  avatar_url?: string;

  user_id?: number;

  static parse(obj: any) {
    const player = new Player();

    if (obj.id) player.id = obj.id;
    if (obj.nickname) player.nickname = obj.nickname;
    if (obj.status) player.status = <Status>obj.status;
    if (obj.ranking) player.ranking = obj.ranking;
    if (obj.avatar_url) player.avatar_url = obj.avatar_url;
    if (obj.user_id) player.user_id = obj.user_id;

    return player;
  }
}
