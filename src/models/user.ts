import { Role } from '@prisma/client';

export class User {
  id?: number;

  email?: string;
  password?: string;
  role?: Role;

  static parse(obj: any) {
    const user = new User();

    if (obj.id) user.id = obj.id;
    if (obj.email) user.email = obj.email;
    if (obj.password) user.password = obj.password;
    if (obj.role) user.role = <Role>obj.role;

    return user;
  }
}
