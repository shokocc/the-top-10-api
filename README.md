<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Description

The top 10 api project

Postman Reference:
> https://www.getpostman.com/collections/f1beb6a5dbbb6aef7c08

### Environment
.env Is used for local commands
```
POSTGRES_USER=admin
POSTGRES_PASSWORD=123456
POSTGRES_DB=topten
DATABASE_URL=postgresql://admin:123456@localhost:5432/topten?schema=topten
POSTGRES_HOST_AUTH_METHOD="trust"
JWT_SECRET=ABCDEFG
CORS_WHITELIST=http://localhost:3000,http://localhost:8080
```

.env.dev Is used for docker and ci/cd flow
```
POSTGRES_USER=admin
POSTGRES_PASSWORD=123456
POSTGRES_DB=topten
DATABASE_URL=postgresql://admin:123456@postgres:5432/topten?schema=topten
POSTGRES_HOST_AUTH_METHOD=trust
JWT_SECRET=ABCDEFG
CORS_WHITELIST=http://localhost:3000,http://localhost:8080
```

### Build

```bash
$ docker-compose build
```

### Database Migrations

```bash
# Run migrations (Development)
$ docker-compose run --rm api npm run migrate:dev

# Apply migrations (Production)
$ docker-compose run --rm api npm run migrate:deploy
```

### Database Seeders

```bash
# Seed database with 3000 fake players
$ docker-compose run --rm api npm run command:seed-players 3000

# Create admin user
$ docker-compose run --rm api npm run command:create-admin <email> <password>
$ docker-compose run --rm api npm run command:create-admin admin@gmail.com 123456
```

### Start
```bash
$ docker-compose up
```

### Tests

```bash
# unit tests
$ docker-compose run --rm api npm run test

# e2e tests
$ docker-compose run --rm api npm run test:e2e

# test coverage
$ docker-compose run --rm api npm run test:cov
```