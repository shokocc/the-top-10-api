import * as path from 'path';
import * as fs from 'fs';
import * as dotenv from 'dotenv';

const envPath = path.join(process.cwd(), '.env.test');

const envConfig = dotenv.parse(fs.readFileSync(envPath));

for (const k in envConfig) {
  process.env[k] = envConfig[k];
}

import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { version, name } from '../package.json';

const storage = {};

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/').expect(200).expect({
      statusCode: 200,
      message: 'Ok',
      data: { version, name },
    });
  });

  it('/api/players (GET)', () => {
    return request(app.getHttpServer())
      .get('/api/players')
      .expect(200)
      .expect({
        statusCode: 200,
        message: 'Ok',
        meta: {
          take: 10,
          skip: 0,
        },
        data: [],
      });
  });

  it('/api/register (POST)', async () => {
    const body = {
      player: {
        nickname: 'Test',
        ranking: 100,
        avatar_url: 'google.com',
      },
      user: {
        email: 'test@gmail.com',
        password: '123456',
      },
    };
    const res = await request(app.getHttpServer())
      .post('/api/auth/register')
      .send(body);
    expect(res.status).toBe(201);
    expect(res.body.data.user).toMatchObject({
      id: 2,
      email: 'test@gmail.com',
      role: 'USER',
      player: null,
    });
    expect(res.body.data.player).toMatchObject({
      id: 1,
      nickname: 'Test',
      status: 'BRONZE',
      ranking: 100,
      avatar_url: 'google.com',
      user_id: 2,
    });
    expect(res.body.data.user.id).toEqual(expect.any(Number));
    expect(res.body.data.player.id).toEqual(expect.any(Number));
    storage['user_id'] = res.body.data.user.id;
    storage['player_id'] = res.body.data.player.id;
  });

  it('/api/login (POST)', async () => {
    const body = {
      email: 'test@gmail.com',
      password: '123456',
    };
    const res = await request(app.getHttpServer())
      .post('/api/auth/login')
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.data).toHaveProperty('access_token');
    expect(res.body.data.user).toMatchObject({
      id: storage['user_id'],
      email: 'test@gmail.com',
      role: 'USER',
      player: {
        id: storage['player_id'],
        nickname: 'Test',
        status: 'BRONZE',
        ranking: 100,
        avatar_url: 'google.com',
        user_id: storage['user_id'],
      },
    });
    storage['access_token'] = res.body.data.access_token;
  });

  it('/api/players (PUT)', async () => {
    const body = {
      player: {
        id: storage['player_id'],
        nickname: 'Test',
        ranking: 100,
        avatar_url: 'google.com',
      },
    };
    const res = await request(app.getHttpServer())
      .put('/api/players')
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.data).toMatchObject({
      id: storage['player_id'],
      nickname: 'Test',
      status: 'BRONZE',
      ranking: 100,
      avatar_url: 'google.com',
      user_id: storage['user_id'],
    });
  });

  it('/api/users/email/check (GET)', async () => {
    const res = await request(app.getHttpServer()).get(
      `/api/users/email/check?email=non_existant_email@gmail.com`,
    );
    expect(res.body).toMatchObject({
      statusCode: 200,
      message: 'Ok',
    });
  });
});
